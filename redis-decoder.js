import {injectPlugin} from "@floocus/spider-plugin";
import { decodeRESP } from './src/parser';

let React;

async function redisDecoder({
   inputs: {payload /* Blob */, direction /* 'out' for request | 'in' for response */},
   parameters: {},
   callbacks: {setDecodedPayload, onShowInfo, onShowError, onShowWarning },
   libs: {React: R, Field, Section, Payload, Divider}
}){
    React = R;

    if(payload) {
        try{
            const reader = new FileReader();
            reader.onload = function () {
                const arrayBuffer = new Uint8Array(reader.result);
                const [decodedMessages, _] = decodeRESP(arrayBuffer);

                if(direction === 'out'){
                    setDecodedPayload(
                        <>
                            {decodedMessages.map((cmd, idx) => (
                                <div key={idx}>
                                    <Command command={cmd[0]} Field={Field} Payload={Payload}/>
                                    <ExtraParams params={cmd.slice(1)} Field={Field}/>
                                    {idx < decodedMessages.length - 1 &&
                                        <Divider/>
                                    }
                                </div>
                            ))}
                        </>
                    );
                }
                else{
                    setDecodedPayload(
                        <>
                            {decodedMessages.flat(Infinity).map((res, idx) => (
                                <div key={idx}>
                                    <Value value={res} Payload={Payload}/>
                                    {idx < decodedMessages.length - 1 &&
                                        <Divider/>
                                    }
                                </div>
                            ))}
                        </>
                    );
                }
            };
            reader.readAsArrayBuffer(payload);
        }
        catch (e){
            onShowError(`Error when parsing: ${e.message}`);
            console.error(e);
        }
    }
}

function Command({command, Field, Payload}){
    const cmd = command[0];
    const params = command.slice(1);
    return (
        <>
            <Field header={'Command'}>
                <b>{cmd.toUpperCase()}</b>
            </Field>
            {params.map((p, idx) => (
                <Field key={idx} header={`Param #${idx}`}>
                    <Value value={p} Payload={Payload}/>
                </Field>
            ))}
        </>
    )
}

function isNumber(value) {
    return typeof value === 'number';
}

function isString(value) {
    return typeof value === 'string';
}

function Value({value, Payload}){
    if(isNumber(value)){
        return value;
    }
    else if(isString(value)){
        if(value.startsWith('{') || value.startsWith('[')){
            try {
                JSON.parse(value);
                return <Payload language={'json'} payload={value}/>
            }
            catch (e){
                return value;
            }
        }
        else{
            return value;
        }
    }
    else{
        return JSON.stringify(value);
    }
}

function ExtraParams({params, Field}){
    return (
        <>
            {params
                .filter(p => p.length)
                .map((p, idx) => (
                    <Field key={idx} header={`Extra param #${idx}`}>{JSON.stringify(p)}</Field>
                ))
            }
        </>
    )
}

injectPlugin({
    id: 'redis-decoder',
    type: 'tcp-payload-decode-plugin',
    version: '0.1',
    func: redisDecoder,
    errorMessage: 'Could not decode payload!'
});