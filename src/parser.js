export function decodeRESP(data) {
  const messages = [];
  let cursor = 0;
  const decoder = new TextDecoder("utf-8");

  while (cursor < data.length) {
    const byte = data[cursor];

    // RESP Simple Strings
    if (byte === 43) { // '+'
      const end = data.indexOf(13, cursor); // looking for '\r'
      if (end === -1) break;
      const text = decoder.decode(data.subarray(cursor + 1, end));
      messages.push(text);
      cursor = end + 2; // skip '\r\n'
    }
    // RESP Errors
    else if (byte === 45) { // '-'
      const end = data.indexOf(13, cursor);
      if (end === -1) break;
      const text = decoder.decode(data.subarray(cursor + 1, end));
      messages.push(text);
      cursor = end + 2;
    }
    // RESP Integers
    else if (byte === 58) { // ':'
      const end = data.indexOf(13, cursor);
      if (end === -1) break;
      const number = parseInt(decoder.decode(data.subarray(cursor + 1, end)), 10);
      messages.push(number);
      cursor = end + 2;
    }
    // RESP Bulk Strings
    else if (byte === 36) { // '$'
      const end = data.indexOf(13, cursor);
      if (end === -1) break;
      const length = parseInt(decoder.decode(data.subarray(cursor + 1, end)), 10);
      cursor = end + 2;
      if (length === -1) {
        messages.push(null);
      } else {
        messages.push(decoder.decode(data.subarray(cursor, cursor + length)));
      }
      cursor += length + 2; // skip the payload and '\r\n'
    }
    // RESP Arrays
    else if (byte === 42) { // '*'
      const end = data.indexOf(13, cursor);
      if (end === -1) break;
      const count = parseInt(decoder.decode(data.subarray(cursor + 1, end)), 10);
      cursor = end + 2;
      const array = [];

      for (let i = 0; i < count; i++) {
        const [decodedMessage, usedBytes] = decodeRESP(data.subarray(cursor));
        array.push(decodedMessage);
        cursor += usedBytes;
      }
      messages.push(array);
    }
    else {
      break; // Invalid or unknown type
    }
  }

  return [messages, cursor];
}